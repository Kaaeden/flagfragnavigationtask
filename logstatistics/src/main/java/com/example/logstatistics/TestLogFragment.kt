package com.example.logstatistics

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy



/**
 * A simple [Fragment] subclass.
 * create an instance of this fragment.
 */
class TestLogFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        println("On Create")
        return ComposeView(requireContext()).apply {
            // Dispose of the Composition when the view's LifecycleOwner
            // is destroyed
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                MaterialTheme {
                    Column(modifier = Modifier.fillMaxSize(), verticalArrangement =Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
                        // In Compose world
                        Text("Hello Compose!")
                    }
                }
            }
        }

    }

    override fun onStart(){
        super.onStart()
        println("On Start")

    }

    override fun onResume() {
        super.onResume()
        println("On Resume")
    }

    override fun onStop() {
        super.onStop()
        println("On Stop")
    }

    override fun onPause() {
        super.onPause()
        println("On Pause")
    }

    override fun onDestroy() {
        super.onDestroy()
        println("On Destroy")
    }

    override fun onDetach() {
        super.onDetach()
        println("On Detach")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        println("On Attach")
    }
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        println("On Create")
    }

}